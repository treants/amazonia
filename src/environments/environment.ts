// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB7pKITcnfjthSfaw8IC68VpboaeEDORXY",
    authDomain: "amazonia-ab647.firebaseapp.com",
    databaseURL: "https://amazonia-ab647.firebaseio.com",
    projectId: "amazonia-ab647",
    storageBucket: "amazonia-ab647.appspot.com",
    messagingSenderId: "507630357909"
  }
};
