/**
 * Created by wangdi on 26/5/17.
 */
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './dashboard/home/home.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import { TableComponent } from './dashboard/table/table.component';
import { NotificationComponent } from './dashboard/notification/notification.component';
import { SweetAlertComponent } from './dashboard/sweetalert/sweetalert.component';
import { SettingsComponent } from './dashboard/settings/settings.component';
import { PriceTableComponent } from './dashboard/component/pricetable/pricetable.component';
import { PanelsComponent } from './dashboard/component/panels/panels.component';
import { WizardComponent } from './dashboard/component/wizard/wizard.component';

import { RootComponent } from './dashboard/root/root.component';
import { LoginComponent } from './page/login/login.component';
import { LockComponent } from './page/lock/lock.component';
import { RegisterComponent } from './page/register/register.component';

//COMPONENTES CREADAS
import { AppHomeComponent } from './pages/home/home.component';
import { AppComplaintsDetailComponent } from './pages/complaints-detail/complaints-detail.component';
import { GeneralMapComponent } from 'app/pages/general-map/general-map.component';
import { AppListarDenunciasComponent } from './dashboard/listar-denuncias/listar-denuncias.component';
import { AppDetalleDenunciasComponent } from './dashboard/detalle-denuncia/detalle-denuncia.component';

const routes: Routes = [
  { path: 'complaint/:id', component: AppComplaintsDetailComponent },
  { path: '', component: AppHomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'lock', component: LockComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: 'dashboard', component: RootComponent, children: [
      { path: 'complaint/:id', component: AppDetalleDenunciasComponent },
      { path: '', component: AppListarDenunciasComponent },
      { path: 'map/condensed', component: GeneralMapComponent },
      { path: 'table', component: TableComponent },
      { path: 'notification', component: NotificationComponent },
      { path: 'alert', component: SweetAlertComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'components/price-table', component: PriceTableComponent },
      { path: 'components/panels', component: PanelsComponent },
      { path: 'components/wizard', component: WizardComponent }
    ]
  }
];

export const routing = RouterModule.forRoot(routes);

