import { Component, OnInit, Inject } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[FormBuilder]
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  pass = false;

  constructor(
    private _db: AngularFirestore,
    private _auth: AngularFireAuth,
    @Inject(FormBuilder) fb: FormBuilder,
    private router: Router
  ) { 
    this.form = fb.group({
      password: ['', Validators.minLength(6)],
      email: '',
    });
  }

  ngOnInit() {
  }

  loginPassword() {
    let success = true
    if(this.form.valid){
      this._auth.auth.signInWithEmailAndPassword(this.form.value.email, this.form.value.password).catch(function(error) {success = false;})
      setTimeout(function(){ if(success) this.router.navigate(['/dashboard']);  }, 1500);
    } 
  }

  loginFacebook(){
    this.authLogin(new firebase.auth.FacebookAuthProvider())
  }


  loginGoogle(){
    this.authLogin(new firebase.auth.GoogleAuthProvider());
  }


  async authLogin(provider){
    let credentials = await this._auth.auth.signInWithPopup(provider)
    
    let user = credentials.user;
    const userRef: AngularFirestoreDocument<any> = this._db.doc(`users/${ user.uid }`);
    const data = {
      uid: user.uid,
      email: user.email,
      name: user.displayName,
      photoURL: user.photoURL
    }
    userRef.set(data)
    this.router.navigate(['/dashboard']); 
  }
}
