import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ComplaintService } from './../../services/complaint.service';
import { Observable } from 'rxjs/Observable';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppVerUbicacionComponent } from './modals/ver-ubicacion/ver-ubicacion.component';
import * as firebase from "firebase";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class AppHomeComponent implements OnInit {
    currentUser;
    complaints: Observable<any[]>;

    constructor(
        private complaintService: ComplaintService,
        private modalService: NgbModal,
        private router: Router
    ) { this.currentUser = firebase.auth().currentUser; }


    ngOnInit() {
        this.complaints = this.complaintService.findAll();
    }

    mostrarUbicacion(latitud, longitud, title) {
        const modalRef = this.modalService.open(AppVerUbicacionComponent);
        modalRef.componentInstance.lat = latitud;
        modalRef.componentInstance.lng = longitud;
        modalRef.componentInstance.title = title;
        modalRef.result.then((data) => {

        })
    }

    mandaDetalle(id: any) {
        this.router.navigate(['complaint', id]);
    }
    goToLogin() {
        this.router.navigate(['/login']);
    }

}
