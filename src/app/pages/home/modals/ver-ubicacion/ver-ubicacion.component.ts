import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-ver-ubicacion',
    templateUrl: './ver-ubicacion.component.html',
    styleUrls: ['./ver-ubicacion.component.scss']
})
export class AppVerUbicacionComponent implements OnInit {

    public lat: any;
    public lng: any;

    constructor(
        private activeModal: NgbActiveModal
    ) { }

    ngOnInit() {

    }

    close() {
        this.activeModal.close();
    }

}
