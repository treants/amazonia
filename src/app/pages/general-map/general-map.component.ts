import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AngularFirestore } from 'angularfire2/firestore';
import { async } from 'q';
import { Router } from '@angular/router';

@Component({
  selector: 'app-general-map',
  templateUrl: './general-map.component.html',
  styleUrls: ['./general-map.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GeneralMapComponent implements OnInit {

  complaints: Observable<any[]>;
  constructor(
    private _db: AngularFirestore,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.complaints = this.findAll();
  }

  findAll(): Observable<any[]> {
    return this._db.collection('/complaints').snapshotChanges()
      .map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      });
  }

  modalPapu(complaint) {
    console.log(complaint);
    this.router.navigate(['/dashboard/complaint', complaint.id])
  }

}
