import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ComplaintService } from './../../services/complaint.service';
import { CommentService } from './../../services/comment.service';
import { Observable } from 'rxjs/Observable';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-complaints-detail',
    templateUrl: './complaints-detail.component.html',
    styleUrls: ['./complaints-detail.component.scss']
})
export class AppComplaintsDetailComponent implements OnInit {

    id: any;
    complaint: Observable<any>;
    comments: Observable<any[]>;
    comment: any = { alias: '', comment: '' };

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private complaintService: ComplaintService,
        private commentService: CommentService
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            params['id'] ? this.id = params['id'] : this.volverInicio();
            this.complaint = this.complaintService.findOne(this.id);
            this.comments = this.commentService.findByComplaint(this.id);
            this.comment.complaint = this.id;
        });
    }

    sendComment() {
        this.commentService.insert(this.comment);
        this.comment = { alias: '', comment: '', complaint: this.id };
    }

    volverInicio() {
        this.router.navigate(['']);
    }

}
