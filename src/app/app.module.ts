import { AgmCoreModule } from '@agm/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireAuthModule } from "angularfire2/auth";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routing } from './app.routes';
import { MatButtonModule, MatRadioModule, MatInputModule, MatMenuModule, MatCheckboxModule } from '@angular/material';

import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HomeComponent } from './dashboard/home/home.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import 'hammerjs';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FigurecardComponent } from './shared/figurecard/figurecard.component';
import { ImagecardComponent } from './shared/imagecard/imagecard.component';
import { TableComponent } from './dashboard/table/table.component';
import { NotificationComponent } from './dashboard/notification/notification.component';
import { MsgIconBtnComponent } from './shared/msgiconbtn/msgiconbtn.component';
import { SweetAlertComponent } from './dashboard/sweetalert/sweetalert.component';
import { LoginComponent } from './page/login/login.component';
import { RootComponent } from './dashboard/root/root.component';
import { RegisterComponent } from './page/register/register.component';
import { LockComponent } from './page/lock/lock.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { SettingsComponent } from './dashboard/settings/settings.component';
import { PriceTableComponent } from './dashboard/component/pricetable/pricetable.component';
import { PanelsComponent } from './dashboard/component/panels/panels.component';

//COMPONENTES CREADAS
import { AppHomeComponent } from './pages/home/home.component';
import { AppComplaintsDetailComponent } from './pages/complaints-detail/complaints-detail.component';
import { AppListarDenunciasComponent } from './dashboard/listar-denuncias/listar-denuncias.component';
import { AppDetalleDenunciasComponent } from './dashboard/detalle-denuncia/detalle-denuncia.component';

//MODALS
import { AppVerUbicacionComponent } from './pages/home/modals/ver-ubicacion/ver-ubicacion.component';
import { AppResponderDenunciaComponent } from './dashboard/listar-denuncias/modals/responder-denuncia/responder-denuncia.component';

//SERVICIOS
import { ComplaintService } from './services/complaint.service';
import { CommentService } from './services/comment.service';

//CONSTANTES
import { environment } from './../environments/environment';

import { SettingsService } from './services/settings.service';
import { WizardComponent } from './dashboard/component/wizard/wizard.component';
import { GeneralMapComponent } from './pages/general-map/general-map.component';
import { ModalManagementStatusComponent } from './modal-management-status/modal-management-status.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HomeComponent,
    ProfileComponent,
    NavbarComponent,
    FigurecardComponent,
    ImagecardComponent,
    TableComponent,
    NotificationComponent,
    MsgIconBtnComponent,
    SweetAlertComponent,
    LoginComponent,
    RootComponent,
    RegisterComponent,
    LockComponent,
    HeaderComponent,
    FooterComponent,
    SettingsComponent,
    PriceTableComponent,
    PanelsComponent,
    WizardComponent,
    AppHomeComponent,
    AppVerUbicacionComponent,
    GeneralMapComponent,
    ModalManagementStatusComponent,
    AppComplaintsDetailComponent,
    AppListarDenunciasComponent,
    AppResponderDenunciaComponent,
    AppDetalleDenunciasComponent
  ],
  entryComponents: [
    AppVerUbicacionComponent,
    AppResponderDenunciaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    BrowserAnimationsModule,
    MatButtonModule,
    MatRadioModule,
    MatInputModule,
    MatMenuModule,
    MatCheckboxModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCjqxqZCAEW17zw0YqIF_4_Hvnq9G-7DI8'
    })
  ],
  providers: [
    SettingsService,
    ComplaintService,
    CommentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
