import { Injectable } from '@angular/core';
import { AngularFirestore } from "angularfire2/firestore";
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';

@Injectable()
export class CommentService {

    constructor(
        private db: AngularFirestore
    ) { }

    findByComplaint(id_complaint): Observable<any[]> {
        return this.db.collection('/comments', ref => ref.where('complaint', '==', id_complaint)).valueChanges();
    }

    insert(comment: any) {
        comment.timestamp = firebase.firestore.FieldValue.serverTimestamp();
        let id = this.db.createId()
        this.db.collection('/comments').doc(id).set(comment);
    }

}