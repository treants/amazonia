import { Injectable } from '@angular/core';
import { AngularFirestore } from "angularfire2/firestore";
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';

@Injectable()
export class ComplaintService {

    constructor(
        private db: AngularFirestore
    ) { }

    findAll(): Observable<any[]> {
        return this.db.collection('/complaints').snapshotChanges()
            .map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                })
            });
    }

    findOne(id): Observable<any> {
        return this.db.doc('/complaints/' + id).valueChanges();
    }

    answer(id, respuesta) {
        let objRespuesta: any = {};
        objRespuesta.timestamp = firebase.firestore.FieldValue.serverTimestamp();
        objRespuesta.description = respuesta;
        let complaint = this.db.doc('/complaints/' + id);
        complaint.update({ answers: objRespuesta });
    }

}
