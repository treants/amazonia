import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-modal-management-status',
  templateUrl: './modal-management-status.component.html',
  styleUrls: ['./modal-management-status.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ModalManagementStatusComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
