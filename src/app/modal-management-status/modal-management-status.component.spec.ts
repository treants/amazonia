import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalManagementStatusComponent } from './modal-management-status.component';

describe('ModalManagementStatusComponent', () => {
  let component: ModalManagementStatusComponent;
  let fixture: ComponentFixture<ModalManagementStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalManagementStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalManagementStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
