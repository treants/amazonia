import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ComplaintService } from './../../../../services/complaint.service';

@Component({
    selector: 'app-responder-denuncia',
    templateUrl: './responder-denuncia.component.html',
    styleUrls: ['./responder-denuncia.component.scss']
})
export class AppResponderDenunciaComponent implements OnInit {

    public id: any;
    respuesta: string = '';

    constructor(
        private activeModal: NgbActiveModal,
        private complaintService: ComplaintService
    ) { }

    ngOnInit() {

    }

    responder() {
        this.complaintService.answer(this.id,this.respuesta);
        this.close();
    }

    close() {
        this.activeModal.close();
    }

}
