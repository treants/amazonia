import { Component, OnInit } from '@angular/core';
import { ComplaintService } from './../../services/complaint.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AppVerUbicacionComponent } from './../../pages/home/modals/ver-ubicacion/ver-ubicacion.component';
import { AppResponderDenunciaComponent } from './modals/responder-denuncia/responder-denuncia.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-listado-denuncias',
    templateUrl: './listar-denuncias.component.html',
    styleUrls: ['./listar-denuncias.component.scss']
})
export class AppListarDenunciasComponent implements OnInit {

    complaints: Observable<any[]>

    constructor(
        private complaintService: ComplaintService,
        private modalService: NgbModal,
        private router:Router
    ) { }

    ngOnInit() {
        this.complaints = this.complaintService.findAll();
    }

    showMaps(latitud, longitud) {
        const modalRef = this.modalService.open(AppVerUbicacionComponent);
        modalRef.componentInstance.lat = latitud;
        modalRef.componentInstance.lng = longitud;
        modalRef.result.then((data) => {

        })
    }

    goToDetail(id) {
        this.router.navigate(['/dashboard/complaint', id]);
    }

    answer(id) {
        const modalRef = this.modalService.open(AppResponderDenunciaComponent);
        modalRef.componentInstance.id = id;
        modalRef.result.then((data) => {

        })
    }

}
