import { Component, OnInit } from '@angular/core';
import { ComplaintService } from './../../services/complaint.service';
import { CommentService } from './../../services/comment.service';
import { Observable } from 'rxjs/Observable';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { AppResponderDenunciaComponent } from './../listar-denuncias/modals/responder-denuncia/responder-denuncia.component';

@Component({
    selector: 'app-detalle-denuncia',
    templateUrl: './detalle-denuncia.component.html',
    styleUrls: ['./detalle-denuncia.component.scss']
})
export class AppDetalleDenunciasComponent implements OnInit {

    id: any;
    complaint: Observable<any>;
    comments: Observable<any[]>;
    comment: any = { alias: '', comment: '' };

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private complaintService: ComplaintService,
        private commentService: CommentService,
        private modalService: NgbModal
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            params['id'] ? this.id = params['id'] : this.volverInicio();
            this.complaint = this.complaintService.findOne(this.id);
            this.comments = this.commentService.findByComplaint(this.id);
            this.comment.complaint = this.id;
        });
    }

    sendComment() {
        this.commentService.insert(this.comment);
        this.comment = { alias: '', comment: '', complaint: this.id };
    }

    volverInicio() {
        this.router.navigate(['']);
    }

    answer() {
        const modalRef = this.modalService.open(AppResponderDenunciaComponent);
        modalRef.componentInstance.id = this.id;
        modalRef.result.then((data) => {

        })
    }

}
